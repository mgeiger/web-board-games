
let http = require('http')

let fs = require('fs')
let path = require('path')
let mime = require('mime-types')

let logger = require('./logger.js')
let consoleCol = require('./consoleColor.js')

let log = msg => logger.log(consoleCol.yellow + "http " + consoleCol.normal + msg)
let err = msg => logger.err(consoleCol.yellow + "http " + consoleCol.normal + msg)

let createHTTPServer = (port) => {
	let server = http.createServer()
	server.listen(port, () => log("started. port " + port))
	server.on('request', (request, response) => {
		let fullpath = __dirname + '/www' + request.url
		let reqIP = request.headers['x-forwarded-for'] || request.connection.remoteAddress
		if (fs.existsSync(fullpath)) {
			let isdir = fs.lstatSync(fullpath).isDirectory()
			let fullpathHTML = fullpath + '/index.html'
			if (!isdir) {
				response.writeHead(200, { "Content-Type": mime.lookup(fullpath) })
				fs.createReadStream(fullpath).pipe(response)
				return
			} else if (isdir && fs.existsSync(fullpathHTML)) {
				log(reqIP + consoleCol.yellow + " " + request.url + consoleCol.normal)
				response.writeHead(200, { "Content-Type": mime.lookup(fullpathHTML) })
				fs.createReadStream(fullpathHTML).pipe(response)
				return
			}
		}
		err(reqIP + " " + consoleCol.yellow + request.url + consoleCol.red + " → 404" + consoleCol.normal)
		response.writeHead(404, { "Content-Type": "text/plain" })
		response.end("ERROR File does not exist")
	})
	return server
}

exports.createHTTPServer = createHTTPServer
