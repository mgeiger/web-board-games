let shuffle = require('./tools.js').shuffle

let allPuppeteers = require("../puppeteer.js").allPuppeteers

let contains = (array, element) => -1 !== array.findIndex(x => x === element)


let Game = function () {

	let players = []
	let playingPuppeteers = () => allPuppeteers.filter(p => contains(players, p.puppet))

	this.list = puppeteer => puppeteer.clientExec('log', players)

	this.join = puppeteer => {
		if (players.filter(u => u == puppeteer.puppet).length == 0) {
			players.push(puppeteer.puppet)
			players = players.sort()
		}
		playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' joined the game'))
	}

	this.leave = puppeteer => {
		players = players.filter(u => u != puppeteer.puppet).sort()
		playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' left the game'))
	}

	this.show = puppeteer => puppeteer.clientExec('log', "la partie n'a pas commencé")
	this.cut = puppeteer => puppeteer.clientExec('log', "la partie n'a pas commencé")

	this.start = (puppeteer) => {
		this.game = new TimeBomb(puppeteer, players)
		this.join = puppeteer => puppeteer.clientExec('log', "cannot join because game started")
		this.leave = puppeteer => puppeteer.clientExec('log', "cannot leave because game started")

		this.show = this.game.show
		this.cut = this.game.cut

	}

}

let TimeBomb = function (puppeteer, players) {

	let playingPuppeteers = () => allPuppeteers.filter(p => contains(players, p.puppet))

	playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' is starting the game'))

	if (players.length < 4) {
		playingPuppeteers().map(p => p.clientExec('log', "need " + (4 - players.length) + " more players to start (min 4 players)"))
		return
	}
	if (players.length > 8) {
		playingPuppeteers().map(p => p.clientExec('log', (players.length - 8) + " players must leave to start (max 8 players)"))
		return
	}


	let distribuer = cards => {
		gamedata.cards = {}
		for (let p in players) {
			gamedata.cards[players[p]] = []
		}
		let i = 0
		while (cards.length) {
			gamedata.cards[players[i % players.length]].push(cards.pop())
			i++
		}
	}

	let good = 'gentil'
	let evil = 'méchant'

	let Card_filMerde = 'fil de merde'
	let Card_filStyle = 'fil stylé'
	let Card_bigben = 'big ben'

	let gamedata = {}
	gamedata.roles = {}

	{
		let roles
		if (players.length == 4 || players.length == 5)
			roles = (new Array(3).fill(good)).concat(new Array(2).fill(evil))
		if (players.length == 6)
			roles = (new Array(4).fill(good)).concat(new Array(2).fill(evil))
		if (players.length == 7 || players.length == 8)
			roles = (new Array(5).fill(good)).concat(new Array(3).fill(evil))
		shuffle(roles)
		for (let i in players) {
			gamedata.roles[players[i]] = roles[i]
		}
	}

	let cards = (new Array(players.length).fill(Card_filStyle))
		.concat(new Array(4 * players.length - 1).fill(Card_filMerde))
		.concat([Card_bigben])

	shuffle(cards)
	distribuer(cards)


	shuffle(players)
	gamedata.pince = players[0]

	gamedata.defausse = []

	this.show = puppeteer => {
		let playersHand = {}
		for (let player of players) {
			playersHand[player] = gamedata.cards[player].length
		}
		puppeteer.clientExec('print', {
			user: puppeteer.puppet,
			role: gamedata.roles[puppeteer.puppet],
			cards: gamedata.cards[puppeteer.puppet],
			pince: gamedata.pince,
			playersHand: playersHand,
			defausse: gamedata.defausse.sort()
		})
	}

	this.cut = (puppeteer, targetPuppet, index) => {
		if (puppeteer.puppet != gamedata.pince) {
			playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' essaye de couper chez ' + targetPuppet + " alors qu'il n'a pas de pince !\nQuel con !"))
			return
		}
		if (puppeteer.puppet == targetPuppet) {
			playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' essaye de couper chez lui même ! Quel boulet !'))
			return
		}
		if (!gamedata.cards[targetPuppet]) {
			puppeteer.clientExec('log', targetPuppet + " n'existe pas dans cette partie, tu as le choix parmis " + players.join(" , "))
			return
		}
		if (!gamedata.cards[targetPuppet][index]) {
			puppeteer.clientExec('log', (targetPuppet + " n'a que " + gamedata.cards[targetPuppet].length + " cartes"))
		}
		let card = gamedata.cards[targetPuppet].splice(index, 1)
		gamedata.pince = targetPuppet
		gamedata.defausse.push(card)
		if (card == Card_bigben) {
			playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' a fait péter la bombe ! BOUM !'))
		}

		let cardLeft = 0
		for (let player of players)
			cardLeft += gamedata.cards[player].length

		if (cardLeft == players.length) {
			playingPuppeteers().map(p => p.clientExec('log', puppeteer.puppet + ' a joué le derier coup, on ne peut plus rien faire ! BOUM !'))
		}

		if (gamedata.defausse.length % players.length == 0) {
			playingPuppeteers().map(p => p.clientExec('log', 'tour terminé ! redistribution des cartes'))
			// shuffle et redistribuer
			let cards = []
			for (let player of players) {
				cards = cards.concat(gamedata.cards[player])
				gamedata.cards[player] = []
			}
			shuffle(cards)
			distribuer(cards)
		}

		allPuppeteers.map(p => p.clientExec('log', puppeteer.puppet + " a trouvé : " + card))
		allPuppeteers.map(p => this.show(p))
		return
	}

	allPuppeteers.map(p => this.show(p))
	playingPuppeteers().map(p => p.clientExec('log', 'game started !'))

}

exports.TimeBomb = TimeBomb
exports.Game = Game
