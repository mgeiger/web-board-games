
let GUI = id => {
	let out = document.getElementById(id)
	if (out) {
		out.toggle = (state, cssClass) => {
			cssClass = cssClass || "hide"
			if (state == true) out.classList.remove(cssClass)
			if (state == false) out.classList.add(cssClass)
			if (state == undefined) out.classList.toggle(cssClass)
		}
	}
	return out
}


{
	let GUItoggleSetup = name => {
		let state = false
		GUI(name).onclick = () => {
			state = !state
			if (state) {
				GUI(name).toggle(!state, "close")
				GUI(name + '-interface').toggle(state)
			} else {
				GUI(name).toggle(!state, "close")
				GUI(name + '-interface').toggle(state)
			}
		}
	}

	GUItoggleSetup('userlist')
	GUItoggleSetup('help')
	GUItoggleSetup('log')
	GUItoggleSetup('gui')

	{
		let state = false
		let loginToggle = s => {
			state = s || !state
			if (state) {
				GUI('login').toggle(!state, "close")
				GUI('login-interface').toggle(state)
				GUI('login-input').focus()
				GUI('login-input').select()
			} else {
				GUI('login').toggle(!state, "close")
				GUI('login-interface').toggle(state)
			}
		}
		GUI('login').onclick = () => { loginToggle() }

		GUI("login-input").addEventListener("keyup", function (event) {
			if (event.keyCode === 13) {
				event.preventDefault()
				let user = GUI("login-input").value
				if (user.length == 0)
					return
				loginToggle(false)
				login(user)
			}
		})
	}
}
