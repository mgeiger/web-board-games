
let Socket = function (socketAddress) {
	let connection = new WebSocket(socketAddress)

	connection.onopen = event => {
		console.log("connected")

		let cookie = {}
		document.cookie.split(/\s*;\s*/).forEach(pair => {
			let p = pair.split(/\s*=\s*/)
			cookie[p[0]] = p.splice(1).join('=')
		})
		if (cookie.login) login(cookie.login)

	}
	// connection.onerror = error => console.log("connexion failed")

	setInterval(() => {
		if (connection.readyState !== 1) {
			GUI("header").style.background = "red"
			GUI("error").innerHTML = "connexion lost"
			GUI("error").toggle(true)
		}
	}, 2000)

	this.execServer = (exec, args) => connection.send(JSON.stringify({ exec: exec, args: args }))

	let actions = {}
	this.bind = (actionName, action) => actions[actionName] = action
	connection.onmessage = msg => {
		msg = JSON.parse(msg.data)
		if (actions[msg.exec])
			actions[msg.exec](msg.args)
	}
}
