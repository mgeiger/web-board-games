"use strict";

let socket = new Socket('ws:' + window.location.href.slice(window.location.href.search('//')))

////////////////////////////////////////////////////////////////////

let log = msg => GUI('log-out').innerHTML += msg + '\n'
let logOneliner = msg => GUI('log-out-oneliner').innerHTML = msg


let print = msg => GUI('console').innerHTML = msg

socket.bind('print', args => {
	log(jsonHighlight(args))
	console.log(args)
	print(jsonHighlight(args))
})

socket.bind('log', args => {
	log(args)
	logOneliner(args)
	console.log(args)
})

let login = user => { socket.execServer("login", user); document.cookie = "login=" + user }
let logout = () => socket.execServer("logout")
let list = user => socket.execServer("list", user)

socket.bind("return.login", args => GUI('username').innerHTML = args)
socket.bind("return.logout", args => GUI('username').innerHTML = "")
socket.bind("return.list", args => console.log(args))

socket.bind("event.list", args => GUI('userlist-out').innerHTML = "<usercount>users(" + args.length + ")</usercount>\n" + args.join("\n"))
socket.bind("event.game.list", args => GUI('gamelist-out').innerHTML = "<usercount>games(" + args.length + ")</usercount>\n" + args.join("\n"))

////////////////////////////////////////////////////////////////////

let game = gamename => {
	if (gamename) {
		let g = {}

		g.list = () => socket.execServer("game", { game: gamename, exec: 'list' })
		g.join = () => socket.execServer("game", { game: gamename, exec: 'join' })
		g.leave = () => socket.execServer("game", { game: gamename, exec: 'leave' })

		// TODO : spécifier quel jeu on veut jouer
		g.start = () => socket.execServer("game", { game: gamename, exec: 'start' })

		// TODO : setup l'API du jeu en question
		g.show = () => socket.execServer("game", { game: gamename, exec: 'show' })
		g.cut = (player, index) => socket.execServer("game", { game: gamename, exec: 'cut', args: { player: player, index: index } })

		return g

	} else {
		// TODO : return list of games
	}

}

let g = game('default')

////////////////////////////////////////////////////////////////////
// testing








// setTimeout(() => {

// 	g.show()
// 	login("laurent");g.join()
// 	login("michel");g.join()
// 	login("bob");g.join()
// 	login("alison");g.join()
// 	g.list()
// 	g.start()
// 	// g.show()

// 	for(let i=0;i<10;i++)g.show()

// },300)
