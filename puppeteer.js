let col = require('./consoleColor.js')
let logger = require('./logger.js')

let allPuppeteers = []

let getPuppets = () => allPuppeteers.map(s => s.puppet).filter(u => u).sort()

let Puppeteer = function (wsServerReq) {
	let connection = wsServerReq.accept(null, wsServerReq.origin)

	allPuppeteers.push(this)

	this.clientExec = (exec, args) => connection.send(JSON.stringify({ exec: exec, args: args }))

	let notifyListChange = () => allPuppeteers.map(sok => sok.clientExec("event.list", getPuppets()))
	// TODO : list users in game that aren't online
	this.clientExec("event.list", getPuppets())
	this.clientExec("event.game.list", [])

	// this.clientExec("event.list",new Array(100).fill("hello"))
	// this.clientExec("event.game.list",new Array(100).fill("hello"))
	// allPuppeteers.map(sok => sok.clientExec("event.game.list",new Array(100).fill("hello")))

	this.logFile = msg => logger.logFile(col.b + "wsok" + col.no + " [" + col.b + wsServerReq.key + col.no + (this.puppet ? ("|" + col.y + this.puppet + col.no) : "") + "] " + msg)
	this.log = msg => logger.log(col.b + "wsok" + col.no + " [" + col.b + wsServerReq.key + col.no + (this.puppet ? ("|" + col.y + this.puppet + col.no) : "") + "] " + msg)
	this.err = msg => logger.err(col.b + "wsok" + col.no + " " + msg)

	let actions = {}
	this.bind = (actionName, action) => actions[actionName] = action
	this.trigger = (actionName, msg) => actions[actionName](msg)
	connection.on('message', msg => {
		msg = JSON.parse(msg.utf8Data)
		if (actions[msg.exec]) {
			this.logFile(JSON.stringify(msg))
			actions[msg.exec](msg.args)
		}
	})

	// ==============================
	// == login logout

	this.bind('login', args => {
		let properName = args.toLowerCase().replace(/[^0-9a-z_\-]/gi, '')
		if (properName && properName != this.puppet) {
			let prevPuppet = this.puppet
			this.puppet = properName
			this.log("login" + (prevPuppet ? (" " + col.y + col.di + prevPuppet + col.normal + " → " + col.y + properName + col.normal) : ""))
			this.clientExec("return.login", properName, true)
			if (prevPuppet)
				allPuppeteers.map(sok => sok.clientExec("log", "change user " + prevPuppet + " → " + properName))
			notifyListChange()
		}
	})

	let logout = () => {
		delete this.puppet
		this.clientExec('return.logout')
		notifyListChange()
	}

	this.bind('logout', args => {
		this.log("logout")
		logout()
	})

	this.bind('list', args => {
		// TODO : if args has a name, list all games it's in
		// TODO : add to this list all players in games that are not online
		this.clientExec('return.list', getPuppets())
	})

	connection.on('close', connection => {
		allPuppeteers.filter(e => e != this)
		this.log("disconnected")
		logout()
	})

	// ==============================
	// == done

	this.log("connected")
}

exports.Puppeteer = Puppeteer
exports.allPuppeteers = allPuppeteers
