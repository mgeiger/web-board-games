"use strict"

// ================
// init + args

process.title = 'web-board-game'

let port = parseInt(process.argv[2]) || 8081

require('readline')
	.createInterface({ input: process.stdin, output: process.stdout, })
	.on('line', line => { try { eval("console.log(" + line + ")") } catch (e) { console.log(e) } })

// ============
// dependencies

let Puppeteer = require("./puppeteer.js").Puppeteer
let createHTTPServer = require("./http.js").createHTTPServer
let webSocketServer = require('websocket').server

// ============
// server setup

let server = createHTTPServer(port)

// ============
// game setup

let Game = require('./games/timebomb.js').Game

let games = {}

// ============
// websocket setup

let wsServer = new webSocketServer({ httpServer: server })
wsServer.on('request', request => {

	let puppeteer = new Puppeteer(request)

	puppeteer.bind('game', args => {

		if (!games[args.game])
			games[args.game] = new Game()

		let g = games[args.game]

		if (args.exec == 'join') g.join(puppeteer)
		if (args.exec == 'leave') g.leave(puppeteer)
		if (args.exec == 'start') g.start(puppeteer)
		if (args.exec == 'list') g.list(puppeteer)
		if (args.exec == 'cut') g.cut(puppeteer, args.args.player, args.args.index)
		if (args.exec == 'show') g.show(puppeteer)

	})

})
