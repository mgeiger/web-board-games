let fs = require('fs');

let dateFormat = require('dateformat')
let consoleCol = require('./consoleColor.js')

let getTimeStr = () => dateFormat(new Date(), "yyyy-mm-dd h:MM:ss")
let getDateStr = () => dateFormat(new Date(), "yyyy-mm-dd")

let log = msg => console.log(consoleCol.yellow + getTimeStr() + consoleCol.normal + " " + msg)
let logFile = msg => fs.appendFile('logs/' + getDateStr() + '.log', msg + '\n', e => { if (e) console.log(e) })

let err = msg => console.log(consoleCol.red + getTimeStr() + consoleCol.normal + " " + msg)

exports.log = log
exports.logFile = logFile
exports.err = err
